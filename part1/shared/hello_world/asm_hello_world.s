;;
;; \file asm_hello_world.s
;;
;; \author Jean-Baptiste Laurent
;; \<jeanbaptiste.laurent.pro@gmail.com\>
;;
;; \date Started on  Fri Dec 26 16:28:06 2014 Jean-Baptiste Laurent
;; \date Last update Thu Jan  8 14:47:45 2015 Jean-Baptiste Laurent
;; \brief File description to place here
;;

[BITS 64]

section .rodata
	my_str: db "Hello world", 0x0a, 0x00

section .text
	extern printf        ; Tell the compiler we will use printf
	global main:function ; Export the 'main' as a function, so we
	                     ; can call it from outside this file.

main:

	;; mov ???, ??? ;; /* There is a missing instruction here */
	mov rdi, my_str
	call printf     ;; printf(...);

	mov rax, 0      ;; return (0);
	ret
