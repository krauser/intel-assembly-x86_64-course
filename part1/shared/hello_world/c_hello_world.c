/*!
** \file hello_world.c
**
** \author Jean-Baptiste Laurent
** \<jeanbaptiste.laurent.pro@gmail.com\>
**
** \date Started on  Thu Aug 21 18:51:08 2014 Jean-Baptiste Laurent
** \date Last update Thu Jan  8 14:48:04 2015 Jean-Baptiste Laurent
** \brief C hello world
*/

#include <stdio.h> /* printf() */

/*!
** @brief A main which simply print hello world.
*/
int	main()
{
  printf("Hello world\n");
  return (0);
}
