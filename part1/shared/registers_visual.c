#include <stdlib.h> /* size_t */
 
 
/* 32     -     16  -  8   -   0 */
/* ----------------------------- */
/* |            EAX            | ; EAX (Extended-AX (32b)) */
/*  ---------------------------  */
/* |....|....|       AX        | ; AX (16b) */
/*  ---------------------------  */
/* |....|....|   AH   |   AL   | ; AH (High) \& AL (Low) */
/* ----------------------------- */
 
 
int      main()
{                        /* MSB             LSB          */
  char   reg[8];         /* -[7 6 5 4 3 2 1 0]-          */
                         /*  -----------------           */
  size_t *rax = &reg[0]; /*  [X|X|X|X|X|X|X|X]-> 8 bytes */
  int    *eax = &reg[0]; /*  [ | | | |X|X|X|X]-> 4 bytes */
  short  *ax  = &reg[0]; /*  [ | | | | | |X|X]-> 2 bytes */
  char   *ah  = &reg[1]; /*  [ | | | | | |X| ]-> 1 byte  */
  char   *al  = &reg[0]; /*  [ | | | | | | |X]-> 1 byte  */
 
  *ah = 15; /* Change also rax, eax, ax and ah           */
  return (0);
}
