;;; \space\space\space\space\space\space$\Rsh$ A register
    add rax, 1 ;; $\rightarrowtail$ An immediate value (a number)
    not [rsp] ;; $\rightarrowtail$ Dereferencing the 'rsp' register
    lea rdi, [rax + 5*2 + '0'] ;; $\rightarrowtail$ Also a dereferencement
;; \space\space\space\space\space\space\space\space\space\space\space\space$\downarrow$
;; An expression with a register, a multiplication and
;; an immediate (a charactere)
;;; ;;;;;;;;;;;;;;;;;
;; \space\space\space\space\space\space$\Rsh$ First parameter, a register
;; \space\space\space\space\space\space$\upharpoonleft$\space\space\space\space$\Rsh$ Second parameter, an immediate value
    mov rax, 0 ;; $\rightarrowtail$ The whole line is the instruction
    ret        ;; $\rightarrowtail$ An instruction without any parameters
;; \space\space\rotatebox[origin=c]{180}{$\Lsh$} The mnemonique
;; $\leftarrow$ A simple comment
;;; ;;;;;;;;;;;;;;;;;
