[BITS 64]

section .rodata
	str:    db "Hello world", 0x0a, 0x00
	len:    equ $-str

section .text
	global main
	extern write

	;; void hello_world(const char *str, size_t len);
hello_world:
	push rbx                ; Backup rbx

	mov rax, rdi            ;
	mov rbx, rsi

	mov rdi, 1              ; param1 = 1;
	mov rsi, rax            ; param2 = (rax = str);
	mov rdx, rbx            ; param3 = (rdx = len);
	call write              ; write(param1, param2, param3);

	pop rbx                 ; Restore rbx
	ret

main:
	mov rdi, str
	mov rsi, len
	call hello_world

	mov rax, 0              ; rax = 0;
	ret                     ; return (rax);
