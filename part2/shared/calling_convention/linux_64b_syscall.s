[BITS 64]

section .rodata
	str:    db "Hello world", 0x0a, 0x00
	len:    equ $-str

section .text
	global main
	extern write

	;; void hello_world(const char *str, size_t len);
hello_world:
	mov rcx, rdi            ;
	mov rdx, rsi

	mov rax, 1              ; fct = write // write syscall number
	mov rdi, 1              ; param1 = 1;
	mov rsi, rcx            ; param2 = (rax = str);
	mov rdx, rdx            ; param3 = (rdx = len);
	syscall                 ; fct(param1, param2, param3);

	ret

main:
	mov rdi, str
	mov rsi, len
	call hello_world

	mov rax, 0              ; rax = 0;
	ret                     ; return (rax);
