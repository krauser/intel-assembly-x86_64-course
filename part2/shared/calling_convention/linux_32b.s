[BITS 32]

section .rodata
	str:    db "Hello world", 0x0a, 0x00
	len:    equ $-str

section .text
	global main
	extern write

	;; void hello_world(const char *str, size_t len);
hello_world:

	mov eax, 4              ; eax = write
	mov ebx, 1              ; param1 = 1
	mov ecx, [esp + 4]      ; param2 = str
	mov edx, [esp + 8]      ; param3 = len
	int 0x80                ; write(param1, param2, param3);

	ret

main:
	                        ; Push argument in reverse order
	push len                ; param2
	push str                ; param1
	call hello_world

	add esp, 8
	mov eax, 0              ; rax = 0;
	ret                     ; return (rax);
