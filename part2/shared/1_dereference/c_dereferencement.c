/*!
** \file 0_dereferencement.c
**
** \author Jean-Baptiste Laurent
** \<jeanbaptiste.laurent.pro@gmail.com\>
**
** \date Started on  Fri Dec 26 14:36:17 2014 Jean-Baptiste Laurent
** \date Last update Thu Jan  8 09:02:35 2015 Jean-Baptiste Laurent
** \brief File description to place here
*/
 
#include <stdio.h>	/* printf() */
 
/*
** [test@localhost [16:11:10] 247]alias disas
** alias disas='objdump -d -M intel '
** [test@localhost [16:11:19] 248]disas ./c_dereferencement
** [...]
** 0000000000400530 <main>:
** 400530:       55                      push   rbp
** 400531:       48 89 e5                mov    rbp,rsp
** 400534:       48 83 ec 20             sub    rsp,0x20
** 400538:       c7 45 e0 01 00 00 00    mov    DWORD PTR [rbp-0x20],0x1
** 40053f:       c7 45 e4 03 00 00 00    mov    DWORD PTR [rbp-0x1c],0x3
** 400546:       c7 45 e8 03 00 00 00    mov    DWORD PTR [rbp-0x18],0x3
** 40054d:       c7 45 ec 07 00 00 00    mov    DWORD PTR [rbp-0x14],0x7
** 400554:       c7 45 f0 ff ff ff ff    mov    DWORD PTR [rbp-0x10],0xffffffff
** 40055b:       c7 45 fc 00 00 00 00    mov    DWORD PTR [rbp-0x4],0x0
** 400562:       eb 21                   jmp    400585 <main+0x55>
** 400564:       8b 45 fc                mov    eax,DWORD PTR [rbp-0x4]
** 400567:       48 98                   cdqe
** 400569:       8b 54 85 e0             mov    edx,DWORD PTR [rbp+rax*4-0x20]
** 40056d:       8b 45 fc                mov    eax,DWORD PTR [rbp-0x4]
** 400570:       89 c6                   mov    esi,eax
** 400572:       bf 30 06 40 00          mov    edi,0x400630
** 400577:       b8 00 00 00 00          mov    eax,0x0
** 40057c:       e8 8f fe ff ff          call   400410 <printf@plt>
** 400581:       83 45 fc 01             add    DWORD PTR [rbp-0x4],0x1
** 400585:       8b 45 fc                mov    eax,DWORD PTR [rbp-0x4]
** 400588:       48 98                   cdqe
** 40058a:       8b 44 85 e0             mov    eax,DWORD PTR [rbp+rax*4-0x20]
** 40058e:       83 f8 ff                cmp    eax,0xffffffff
** 400591:       75 d1                   jne    400564 <main+0x34>
** 400593:       b8 00 00 00 00          mov    eax,0x0
** 400598:       c9                      leave
** 400599:       c3                      ret
** 40059a:       66 0f 1f 44 00 00       nop    WORD PTR [rax+rax*1+0x0]
** [...]
*/
 
int	main()
{
  int	cpt;
  int	tab[5];
 
  tab[0] = 1;
  tab[1] = 3;
  tab[2] = 3;
  tab[3] = 7;
  tab[4] = -1;
  cpt = 0;
  while (tab[cpt] != -1)
  {
    printf("Tab[%d] = [%d]\n", cpt, tab[cpt]);
    cpt = cpt + 1;
  }
  return (0);
}
