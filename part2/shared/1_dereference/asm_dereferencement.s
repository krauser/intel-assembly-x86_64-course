;;
;; \file 0_dereferencement.s
;;
;; \author Jean-Baptiste Laurent
;; \<jeanbaptiste.laurent.pro@gmail.com\>
;;
;; \date Started on  Fri Dec 26 14:38:13 2014 Jean-Baptiste Laurent
;; \date Last update Thu Jan  8 09:06:19 2015 Jean-Baptiste Laurent
;; \brief File description to place here
;;

[BITS 64]
	;;
%define INT_SIZE 4
%define TAB_SIZE 5 * INT_SIZE
	;;
section .rodata
	tab:	db "Tab[%d] = [%d]", 0x0a, 0x00
	;;
section .text
	extern printf
	global main:function
	;;
	;; A little remainder for the Nasm size specifiers:
	;; 		 byte  = size 1 /* char    */
	;;		 word  = size 2 /* short   */
	;; (double word) dword = size 4 /* int     */
	;; (  quad word) qword = size 8 /* ssize_t */
	;;
main:
	push rbp          ;; Prolog
	mov rbp, rsp      ;;
	sub rsp, TAB_SIZE ;; int tab[4]; ;; Allocate 4 int on stack
	;;
	mov eax, -1
	mov [rsp + 4*0], dword 1 ;; tab[0] = 1;
	mov [rsp + 4*1], dword 3 ;; tab[1] = 3;
	mov [rsp + 4*2], dword 3 ;; tab[2] = 3;
	mov [rsp + 4*3], dword 7 ;; tab[3] = 7;
	mov [rsp + 4*4], eax     ;; tab[4] = -1;
	;; Warning: we use 'eax' and not 'rax'.
	;; Nasm, when the size specifier is not present,
	;; use the register size.
	;; With 'rax' 8 bytes would have been moved instead of 4 !
	;;
	mov rcx, 0               ;; int cpt = 0;
.loop:                           ;; Sublabel .loop == main.loop
	cmp [rsp + rcx * 4], dword -1 ;; We compare an int of 4 bytes
	je .endloop
	;;
	mov rax, 0               ;; No floating argument
	mov edx, [rsp + 4 * rcx] ;; Init third parameter  (int)
	mov esi, ecx             ;; Init second parameter (int)
	mov rdi, tab             ;; Init first parameter  (char const *)
	push rcx                 ;;
	call printf              ;; printf("Ta...", tab);
	pop rcx
	;;
	inc rcx
	jmp .loop
.endloop:
	mov rsp, rbp            ;; Clear current stack frame
	pop rbp                 ;; Restore old stack frame
	mov rax, 0
	ret                     ;; return (0);
