;;
;; \file 0_dereferencement.s
;;
;; \author Jean-Baptiste Laurent
;; \<jeanbaptiste.laurent.pro@gmail.com\>
;;
;; \date Started on  Fri Dec 26 14:38:13 2014 Jean-Baptiste Laurent
;; \date Last update Thu Jan  8 09:05:55 2015 Jean-Baptiste Laurent
;; \brief File description to place here
;;

[BITS 64]
	;;
section .rodata
	tab:	db "Tab = [%s]", 0x0a, 0x00
	;;
section .text
	extern printf
	global main:function
	;;
main:
	sub rsp, 5               ;; char tab[5]; Allocate 5 bytes on stack
	;;
	mov [rsp + 0], byte 'G'  ;; tab[0] = 'G';
	mov [rsp + 1], byte 'o'  ;; tab[1] = 'o';
	mov [rsp + 2], byte 'o'  ;; tab[2] = 'o';
	mov [rsp + 3], byte 'd'  ;; tab[3] = 'd';
	mov [rsp + 4], byte 0x00 ;; tab[4] = '\0';
	;;
	mov rax, 0
	mov rdi, tab
	mov rsi, rsp
	call printf              ;; printf("Ta...", tab);
	;;
	add rsp, 5               ;; Clear tab[] from the stack
                                 ;; like free() for malloc()
	mov rax, 0
	ret                      ;; return (0);
