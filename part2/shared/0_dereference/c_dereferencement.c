/*!
** \file 0_dereferencement.c
**
** \author Jean-Baptiste Laurent
** \<jeanbaptiste.laurent.pro@gmail.com\>
**
** \date Started on  Fri Dec 26 14:36:17 2014 Jean-Baptiste Laurent
** \date Last update Thu Jan  8 09:02:27 2015 Jean-Baptiste Laurent
** \brief File description to place here
*/
 
#include <stdio.h>	/* printf() */
 
/*
** [test@localhost [14:58:00] 147]alias disas
** alias disas='objdump -d -M intel '
** [test@localhost [14:58:10] 148]disas ./c_dereferencement
**
** [...]
** 0000000000400530 <main>:
** 400530:       55                      push   rbp
** 400531:       48 89 e5                mov    rbp,rsp
** 400534:       48 83 ec 10             sub    rsp,0x10
** 400538:       c6 45 f0 47             mov    BYTE PTR [rbp-0x10],0x47
** 40053c:       c6 45 f1 6f             mov    BYTE PTR [rbp-0xf],0x6f
** 400540:       c6 45 f2 6f             mov    BYTE PTR [rbp-0xe],0x6f
** 400544:       c6 45 f3 64             mov    BYTE PTR [rbp-0xd],0x64
** 400548:       c6 45 f4 00             mov    BYTE PTR [rbp-0xc],0x0
** 40054c:       48 8d 45 f0             lea    rax,[rbp-0x10]
** 400550:       48 89 c6                mov    rsi,rax
** 400553:       bf 00 06 40 00          mov    edi,0x400600
** 400558:       b8 00 00 00 00          mov    eax,0x0
** 40055d:       e8 ae fe ff ff          call   400410 <printf@plt>
** 400562:       b8 00 00 00 00          mov    eax,0x0
** 400567:       c9                      leave
** 400568:       c3                      ret
** 400569:       0f 1f 80 00 00 00 00    nop    DWORD PTR [rax+0x0]
** [...]
*/
 
int	main()
{
  char	tab[5];
 
  tab[0] = 'G';
  tab[1] = 'o';
  tab[2] = 'o';
  tab[3] = 'd';
  tab[4] = '\0';
  printf("Tab = [%s]\n", tab);
  return (0);
}
