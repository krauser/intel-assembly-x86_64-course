
--> Code en step by step (un code par notion) Puis un code avec tout pour le recap ?
--> Utiliser les couleurs avec un Makefile (make loop / make deref / make memory / etc)
--> Coder en live ? (comme les videos koala) ou preparer le code en avance (ou les 2 ?)

2/ Notions standards (15/20min)
----------------------------
----
## Notes internes:
* Notions abordees dans cette video:

// Transition
* * Faire un appel système
* * labels
* * Fonction (hello world qui prend un parametre et return le retour de write)
* * * Paramètre 64b
* * * Retour (rappel)
* * Convention d'appel

* * Branchement
* * * Les conditions
* * * Les eflags
* * * Les boucles

* * Utiliser la mémoire
* * * Stack
* * * Heap
* * * Dereferencement

* * Compiler une bibliotheque en .so

################################# Appel système

   # Syscall
   ## Instruction syscall (x64) (+ seulement mentionner "int 0x80" et la doc)
   ## Convention d'appel pour int 0x80 et syscall
   ### int 0x80 --> eax, ebx, ecx, etc
   #### 1e methode pour un syscall, génère une interruption (lent)
   ### syscall --> rdi, rsi, rdx, etc
   #### Plus d'interuption, beaucoup d'assomption, plus rapide
   #### Pour simplifier, syscall est l'équivalent AMD du sysenter Intel
   ## Pourquoi/difference int 0x80 et non syscall
   ### Les numeros de syscall ne sont pas les mêmes

################################ Fonction / label / convention d'appel / etc

   # Fonction / label / sous-label
   ## Parametres
   ## Retours
   --> Il est possible d'appeler une fonction avec un return
   --> call/ret sont lies mais ce n'est que convention pour faciliter le code.
   # Argc/Argv (parametre du main, qui est une fonction)
   --> Deduction: main est une fonction qui prend 0/2/3 parametres
                  L'exercice est laisse a l'etudiant
   # Convention d'appel (32b (cdecl)/64b (SystemV AMD64 ABI))
   ## Convention Cdecl 32b (stack)
   ## Convention SystemV AMD64 ABI (registre)

################################# Branchement

   ## Condition/Comparaison
   ### Code a court circuit par rapport au language C courant (if / while / for)
   --> Expliquer la difference entre les deux
   --> Poser un exemple
   ### Danger des flags (verifier que personne entre la comparaison et le jcc les modifies)
   --> Poser un exemple (if / else)
   --> mentionner les cmovcc/BMI1&2 dans les slides
   //   --> Instruction avancee pour cette utilisation + optimisation
   //    (Bit Manipulation Instruction (BMI1/2)) (Intel xeon)
   // (Evoquer directement l'instruction est facultatif)
   // ---> SAL/SAR/SHL/SHR -- Shift
   // ---> SARX/SHLX/SHRX -- Shift Without Affecting Flags
   // --> TODO Exemple

   ## Boucles
   --> Convention: Au lieu de dire, je compte jusqu'a 10 (0-9), l'on dit:
                  Il me reste 10 elements a compter (9-0)
       Montrer du code et des instructions specialise pour ca.
   --> TODO Exemple
   ### While
   ### For

################################# Memoire

   # Memoire
   ## Heap/Stack
   ### Stack
   #### Definition
   #### Utilisation de la stack
   --> Contexte de stack (registres bp/sp)
   --> Explicite: Push / Pop
   --> Implicite: call / leave / ret
   -->    Manuel: add / sub / etc... (modification du registre)
   --> TODO Exemple
   #### Culture general par rapport aux stacks des autres assembleur
   --> "Increment / Decrement mode stacks (empty mode / full mode)"

   ### Heap
   --> Meme utilisation que en C (via syscall ou un allocation (ex: malloc))
   --> Manuel Intel "Volume 1 : Chapter 6 Procedure calls, interrupts, and exceptions" (6.1, 6.2)
   --> TODO Exemple

   # Dereferencement
   ## Rappel succins des pointeurs ('&' et '*')
   ## Comparaison par rapport a l'assembleur
   --> Comparaison: '&' ==> 'lea', '*' ==> '[]'
   --> Warning: Un registre n'a pas d'adresse (Il n'est pas dans la RAM, mais en interne du processeur),
                seul la memoire en a.
   --> TODO Exemple
   ## Pitfall par rapport aux types et leurs taille (qualifier byte->qword)
   --> TODO Exemple
   --> TODO ajouter de la documentation
   # Modrm/SIB (a verifier si besoin dans le module) (sur slide, comme au DF5)
   --> [ptr]               ==> *ptr
   --> [ptr + 5]           ==> *(ptr + 5)
   --> [ptr + 4*it]        ==> *(ptr + (sizeof(int) * it))
   --> [ptr + 4*it + rest] ==> (ptr + (sizeof(int) * it)).str[0]
   --> (tableau de structure)



################################# Faire une bibliotheque

   # PLT/GOT (ne pas entamer les notions du strace/ftrace)
   ## Definition officiel
   ## Reexplication et justification
   --> Quelles problemes y aurait'il si la PLT n'existait pas, si la GOT n'existait pas
