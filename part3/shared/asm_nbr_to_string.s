[BITS 64]
	;;
section .data
	_str:		times 12 db 0  ; "-2147483648"\0 = 12 bytes
	_strlen:	equ	$-_str ; The size of the _str array.
section .text
	;;
	global nbr_to_string:function
	extern malloc
	extern memset
	;;
_clear_str:                 ; A wrapper to memset()
	mov rsi, 0
	mov rdx, _strlen
	call memset
	ret
	;;
_recur:                      ; int _recur(int nbr, int sign)
	cmp edi, 0           ; Compare
	jz ._unstack_recur   ;
	;;
._stack_recur:               ; if (rdi != 0) /* Recursion */
	mov ecx, 10          ; Division value
	mov eax, edi         ; Implicite idiv register
	;;
	mov edx, edi         ; (nbr > 0 ? 0 : -1)
	sar edx, 31          ; Extend edx with sign
	idiv ecx             ; nbr / 10
	push rdx             ; push remainder
	;;
	mov edi, eax
	call _recur          ; Recursion, (_recur(nbr / 10))
	;;
	pop rdx              ; Get the remainder (rdx = (nbr % 10))
	neg dl               ; (remainder is negative, inverse sign)
	add dl, '0'          ; remainder + '0'
	mov [_str + eax], dl ; str[eax] = dl
	inc eax              ;
	ret                  ; return (eax + 1)
._unstack_recur:             ; if (rdi == 0) /* End of recursion */
	mov al, ','          ;; Ascii trick to get the sign character
	sub al, sil          ;; (43:'+' | 44:',' | 45:'-')
	mov [_str], al
	mov eax, 1
	ret
	;;
nbr_to_string:               ; char *nbr_to_string(int nbr)
	push rdi             ; Save nbr (changed by memset)
	mov rdi, _str
	call _clear_str      ; Clear str with zero
	pop rdi              ; Restore nbr
	;;
	mov esi, 1
	cmp edi, 0
	jz ._zero
	jl ._negative
._positive:                  ; if (nbr > 0)
	neg edi              ;   nbr = -nbr;
	jmp ._end_signed
._negative:                  ; if (nbr < 0)
	neg esi              ;   sign = -sign;
._end_signed:
	call _recur
._end:                       ; Return the string from nbr_to_string()
	mov rax, _str
	ret
	;;
._zero:                      ; if (nbr == 0) /* Just return "0" */
	xor ecx, ecx
	mov cl, '0'
	mov [rax], word cx
	jmp ._end
