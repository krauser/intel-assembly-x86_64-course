/*!
** \file nbr_to_string.c
**
** \author Jean-Baptiste Laurent
** \<jeanbaptiste.laurent.pro@gmail.com\>
**
** \date Started on  Thu Jul 10 20:01:54 2014 Jean-Baptiste Laurent
** \date Last update Thu Jan  8 09:02:53 2015 Jean-Baptiste Laurent
** \brief File description to place here
*/
 
#include <stdio.h>	/* printf() */
 
static char	str[11];
 
static int	_recur(int nbr, int sign)
{
  int		it;
  int		result;
 
  it = 0;
  if (nbr)
  {
    it = _recur(nbr / 10, sign);
    str[it] = -(nbr % 10) + '0';
  }
  else  /* Ascii trick to get the sign character (sign == 1 or -1) */
    str[0] = ',' - sign; /* (43:'+' | 44:',' | 45:'-') */
  return (it + 1);
}
 
char	*nbr_to_string(int nbr)
{
  if (nbr < 0)
    _recur(nbr, -1);
  else if (nbr > 0)
    _recur(-nbr, 1);
  else
    str[0] = '0';
  return (&str[0]);
}
 
int	main(int argc, char **argv)
{
  int	nbr;
 
  nbr = atoi(argv[1]);
  printf("%s\n", nbr_to_string(nbr));
  return (0);
}
