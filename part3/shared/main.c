/*!
** \file main.c
**
** \author Jean-Baptiste Laurent
** \<jeanbaptiste.laurent.pro@gmail.com\>
**
** \date Started on  Thu Jul 10 18:33:03 2014 Jean-Baptiste Laurent
** \date Last update Wed Jan 14 10:38:18 2015 Jean-Baptiste Laurent
** \brief Assembly class: main
*/

#include <stdio.h>
#include <stdlib.h>

char	*nbr_to_string(int nbr);

int	main(int argc, char **argv)
{
  int	nbr;
  int	cpt;
  char	*str;

  nbr = 0;
  cpt = 0;
  if (argc > 1)
  {
    while (cpt < argc)
    {
      nbr += strtol(argv[cpt], NULL, 10);
      cpt = cpt + 1;
    }
    str = nbr_to_string(nbr);
    if (str)
    {
      printf("Printing the sum of all number in argv\n");
      printf("==> Sum = %s\n", str);
      return (EXIT_SUCCESS);
    }
  }
  else
    fprintf(stderr, "Usage: %s nbr1 [nbr2]*\n", argv[0]);
  return (EXIT_FAILURE);
}
