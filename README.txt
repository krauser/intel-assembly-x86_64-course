This folder architecture is organized as followed:

part1/-  ------------------------- The first part of this course
├── pdf_document  ------------------ The latex PDF complete document
│   └── tex_sources  ----------------- Sources of the latex pdf
├── shared  ---------------------- Data shared between part1 video, slide and pdf
│   └── hello_world  --------------- Some folder to organize files
├── slide  ----------------------- The slide associated with the video
│   └── slide_img  ----------------- Image included into the slide
└── video  ----------------------- The video itself
    └── slides_videodialogue  ------ The dialogue of the speaker during the video

part2:
...
part3:
...

shared/ ------------------------- Data shared between multiple part
